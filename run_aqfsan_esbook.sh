#!/usr/bin/env bash

# file = run_aqf_esbook.bat <pub> <pro> <ms>
ms  = $3
pro = $2
pub = $1

make -f production.Makefile pro=$pro pub=$pub ms=$ms
if [[ $? -gt 0 ]]
then
   echo "error" >>aqfsan.log
fi

