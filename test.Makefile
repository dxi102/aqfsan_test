
# file = run_aqf_esbook.bat <pub> <pro> <ms>

pub = eschbook
pro = carcione
ms  = carcione02

template = esbook
log = aqfsan.log

CONFIGURE = ruby /home/deimi/local/gitlab/aqfsan/configure.rb

0:
	getfile --loose $(ms)

1:
	rm -f $(log)
	touch $(log)
	$(CONFIGURE) --pub=$(pub) --pro=$(pro) --ms=$(ms) \
               --makefile-template=$(template).Makefile.mustache \
               --frontcover-template=$(template).cover.mustache \
               --msextrafile-template=$(template).queries.mustache
	cp ../aqfsan/tex/esbook.queries.tex queries.tex
	cp ../aqfsan/tex/esbook.texdist.cfg texdist.cfg
	make
#	make clean
	grep -i "fatal" config.log || true >> $(log)
	grep -i "error" config.log || true >> $(log)

clean:
	rm -f $(ms).*
	rm -f *.mustache
	rm -f data.yml
	rm -f aqfsan.log config.log
	rm -f Makefile
	rm -f aqf.* query.* texdist.cfg queries.tex
	rm -rf style
