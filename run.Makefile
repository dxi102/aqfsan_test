SHELL = /bin/bash
# $ run_aqf_esbook.bat <pub> <pro> <ms>

pub = eschbook
pro = carcione
ms  = carcione02
# make -f run.Makefile pub=eschbook pro=carcione ms=carcione02

template = esbook
log = aqfsan.log

CONFIGURE = ruby /home/deimi/local/gitlab/aqfsan/configure.rb
EXTRATEXDIR = /home/deimi/local/gitlab/aqfsan/tex

default: help
production: 1

0:
	getfile --loose $(ms)

1:
	rm -f $(log)
	touch $(log)
	$(CONFIGURE) --pub=$(pub) --pro=$(pro) --ms=$(ms) \
               --makefile-template=$(template).Makefile.mustache \
               --frontcover-template=$(template).cover.mustache \
               --msextrafile-template=$(template).queries.mustache
#
# These two files probably should be implemented
# into cls/sty files.
# But perhaps these files are needed during
# transition period.
	cp $(EXTRATEXDIR)/esbook.queries.tex queries.tex
	cp $(EXTRATEXDIR)/esbook.texdist.cfg texdist.cfg
	make
#	make clean
	grep -i "fatal" config.log || true >> $(log)
	grep -i "error" config.log || true >> $(log)

distclean: clean
	rm -f $(ms).dvi $(ms).pdf
	rm -f query.pdf

clean:
# remove $ms.* except $ms.dvi and $ms.pdf
	find ./ -type f  -name '$(ms).*' -not -name '$(ms).dvi' -not -name '$(ms).pdf' -print0 | xargs -0  -I {} rm {}
	rm -f *.mustache
	rm -f data.yml
	rm -f aqfsan.log config.log
	rm -f Makefile
	rm -f aqf.* texdist.cfg queries.tex
	rm -rf style
# remove query.* except query.pdf
	find ./ -type f  -name 'query.*' -not -name 'query.pdf' -print0 | xargs -0  -I {} rm {}


help:
	@printf "Help message\n"
	@printf "============\n\n"
	@printf "<TODO>\n"

